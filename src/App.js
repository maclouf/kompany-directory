import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Tabletop from 'Tabletop';

function App() {
  const [sheetData, setSheetData] = useState([]);
  useEffect(() => {
      Tabletop.init({
        key: '11BI9vsKXt-GWGW_-sAeKCmkyXR4eA4VxZQ17RI3v1qw',
        callback: googleData => {
          setSheetData(googleData);
        },
        simpleSheet: true
      })
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
